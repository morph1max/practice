import 'package:flutter/material.dart';
import 'package:practice/const.dart';
import 'package:practice/pages/home.dart';
import 'package:practice/pages/notebook.dart';

import 'pages/game.dart';
import 'pages/map.dart';


void main() {
  runApp(
    MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: lowBlue,
        appBarTheme: AppBarTheme(
          backgroundColor: mainBlue,
          titleTextStyle: const TextStyle(
            fontSize: 30,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
          ),
        ),
        textTheme: const TextTheme(
          bodyMedium: TextStyle(
            fontSize: 25,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w500,
          ),
          bodySmall: TextStyle(
            fontSize: 20,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w100,
          ),
          labelMedium: TextStyle(
            fontSize: 20,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w100,
          ),
        ),
        
      ),

      routes: {
        '/': (context) => const HomePage(),
        '/notebook': (context) => const NotebookPage(),
        '/map': (context) => const MapPage(),
        '/game': (context) => const GamePage(),
      },
      initialRoute: '/',
    )
  );
}
