import 'package:flutter/material.dart';
import 'package:practice/gameFiles/sprite.dart';


Color lowBlue = const Color.fromARGB(255, 241, 248, 252);
Color mainBlue = const Color.fromARGB(255, 154, 206, 235);
Color mainOrange = const Color.fromARGB(255, 255, 202, 134);

Sprite dino = Sprite("assets/dino_1.png", 88, 94);
Sprite spriteCactus = Sprite("assets/cactus.png", 180, 130);

// Dino
double GRAVITY_KOEF = 1500;
double JUMP_HEIGHT = 750;
double SPEED_DINO = 0.2;

// Cactus
double SPEED_CACTUS = 70;
double MAX_DIST_CACTUS_SPAWN = 23;
