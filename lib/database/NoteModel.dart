class NoteModel {
  final String text;

  NoteModel({required this.text});

  Map<String, String> toMap() {
    return {
      'text': text,
    };
  }

  @override
  String toString() {
    return 'NoteModel(text: $text)';
  }

}