import 'package:practice/database/NoteModel.dart';
import 'package:sqflite/sqflite.dart';


class CRUD {

  Future<void> insertNote(Future<Database> database, NoteModel noteModel) async {
    final db = await database;
    
    await db.insert(
      'noteModel',
      noteModel.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<String> readNote(Future<Database> database) async {
    final db = await database;

    final List<Map<String, dynamic>> maps = await db.query('noteModel');

    if (maps.isEmpty) {
      insertNote(database, NoteModel(text: ""));
    }

    return maps[0]['text'];
  }

  Future<void> updateNote(Future<Database> database, NoteModel noteModel) async {
    final db = await database;
    await db.update(
      'noteModel',
      noteModel.toMap(),
    );
  }

  Future<void> deleteNote(Future<Database> database) async {
    final db = await database;

    // Remove the Dog from the database.
    await db.delete(
      'noteModel',
    );
  }
}
