import 'package:flutter/widgets.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';


Future<Database> connect() async {

  WidgetsFlutterBinding.ensureInitialized();
  // Open the database and store the reference.
  return openDatabase(
    // Set the path to the database. Note: Using the `join` function from the
    // `path` package is best practice to ensure the path is correctly
    // constructed for each platform.
    join(await getDatabasesPath(), 'database1.db'),

    onCreate: (db, version) {
    return db.execute(
      'CREATE TABLE noteModel(text TEXT)'
      );
    },

    version: 1,
  );
}