import 'package:flutter/material.dart';
import 'package:practice/const.dart';

import 'package:practice/database/CRUD.dart';
import 'package:practice/database/Connection.dart';
import 'package:practice/database/NoteModel.dart';


class NotebookPage extends StatefulWidget {
  const NotebookPage({super.key});

  @override
  State<NotebookPage> createState() => _NotebookPageState();
}


class _NotebookPageState extends State<NotebookPage> {
  
  final database = connect();
  CRUD crud = CRUD();

  var myController = TextEditingController();
  
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  Future<String> readNode() async {

    String text = await crud.readNote(database);

    myController = TextEditingController(text: text);
    return text;
  }

  @override
  Widget build(BuildContext context) {

    final theme = Theme.of(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("БЛОКНОТ"),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            size: 35,
            color: Colors.black,
          ),
          onPressed: () {
            crud.updateNote(database, NoteModel(text: myController.text));
            Navigator.pushReplacementNamed(context, '/');
          }
        ),
      ),

      body: ListView(
        children: [
          const SizedBox(height: 60),

          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30),
              
              child: Container(
                padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
                height: MediaQuery.of(context).size.height * 0.80,
                color: mainBlue,
                
                child: FutureBuilder(
                  future: readNode(),
                  initialData: "Загрузка...",
                  builder: (BuildContext context, AsyncSnapshot<String> text) {
                    return TextFormField(
                      keyboardType: TextInputType.multiline,
                      controller: myController,
                      maxLines: null,
                      style: const TextStyle(
                        fontSize: 25,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w300,
                      ),

                      decoration: const InputDecoration(
                        hintText: 'Напишите что-нибудь:',
                        hintStyle: TextStyle(
                          color: Color.fromARGB(255, 106, 106, 106),
                          fontSize: 25,
                          fontFamily: 'Roboto',
                        ),

                      ),
                    );
                  }
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
