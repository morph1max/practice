import 'dart:math';

import 'package:flutter/material.dart';
import 'package:practice/const.dart';
import 'package:practice/gameFiles/gameObjects/gameObject.dart';
import 'package:practice/gameFiles/gameObjects/cactus.dart';
import 'package:practice/gameFiles/gameObjects/dino.dart';


// https://www.thkp.co/blog/2020/10/19/building-the-chrome-dino-game-from-scratch-in-flutter
class GamePage extends StatefulWidget {
  const GamePage({super.key});

  @override
  State<GamePage> createState() => _GamePageState();
}


class _GamePageState extends State<GamePage> with TickerProviderStateMixin {

  Dino dino = Dino();
  Cactus obstacle = Cactus(Offset(MAX_DIST_CACTUS_SPAWN, 0));

  double runDistance = 0; 
  double runSpeed = 5;

  late AnimationController worldController;
  Duration lastUpdateCall = const Duration();

  // Фиксирую точку левого края дино.
  double denoRectTop = 0;

  @override
  Widget build(BuildContext context) {

    Size screenSize = Size(
      MediaQuery.of(context).size.width,
      MediaQuery.of(context).size.height
    );

    // Добавление динозавра и кактуса на экран.
    List<Widget> listGameObjects = [];
    for (GameObject object in [obstacle, dino]) {
      listGameObjects.add(
        AnimatedBuilder(
          animation: worldController,
          builder: (context, child) {
            Rect objectRect = object.getRect(screenSize, runDistance);
            return Positioned(
                top: objectRect.top,
                left: objectRect.left,
                width: objectRect.width,
                height: objectRect.height,
                child: object.render()
            );
          }
        ),
      );
    }

    Rect denoRect = dino.getRect(screenSize, runDistance);
    if (denoRectTop == 0) {
      denoRectTop = denoRect.top;
    }
    listGameObjects.add(
      Padding(
        padding: EdgeInsets.fromLTRB(0, denoRectTop + denoRect.height, 0, 0),
        child: Container(
          color: mainBlue,
          width: screenSize.width,
          height: screenSize.height - denoRectTop,
          child: Align(
            alignment: Alignment.center,
            child: Text('Очки: ${double.parse((runDistance).toStringAsFixed(3))}')
          ),
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("ИГРА"),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            size: 35,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/');
          }
        ),
      ),

      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          dino.jump();
        },
        child: Stack(
          alignment: Alignment.center,
          children: listGameObjects
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    worldController = AnimationController(
      vsync: this,
      duration: const Duration(days: 99)
    );

    worldController.addListener(_update);
    worldController.forward();
  }

  /* Обновление всеееех данных игры и перерисовка объектов. */
  void _update() {

    // Обнолвение скорости и дистанции.
    double elapsedSeconds =
        ((worldController.lastElapsedDuration!.inMilliseconds -
                lastUpdateCall.inMilliseconds) /
            1000);
    runDistance = max(runDistance + runSpeed * elapsedSeconds, 0);
    runSpeed = runSpeed + SPEED_DINO * elapsedSeconds;

    // Обнолвение данных у динозавра.
    dino.update(lastUpdateCall, worldController.lastElapsedDuration);
    lastUpdateCall = worldController.lastElapsedDuration!;

    // Проверка на попадание в кактус.
    Size screenSize = MediaQuery.of(context).size;
    Rect dinoRect = dino.getRect(screenSize, runDistance).deflate(15);
    Rect obstacleRect = obstacle.getRect(screenSize, runDistance);
    if (dinoRect.overlaps(obstacleRect.deflate(15))) {
      _die();
    }

    // Генерируем новый кактус, если пробелажи прошлый.
    if (obstacleRect.right < 0) {
      setState(() {
        obstacle = Cactus(Offset(runDistance + Random().nextInt(MAX_DIST_CACTUS_SPAWN.toInt()) + 15, 0));
      });
    }
  }


  /* Функия срабатывает, когда дино попал в кактус.
  Конец игры. */
  void _die() {
    setState(() {
      worldController.stop();
      dino.die();
    });
  }

}
