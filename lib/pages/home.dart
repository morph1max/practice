import 'package:weather/weather.dart';
import 'package:flutter/material.dart';
import 'package:practice/const.dart';


class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {

  WeatherFactory wf = WeatherFactory(
    "4d0dbd12a1bc565d0298dd6371caf790",
    language: Language.RUSSIAN
  );


  Future<String?> readNode() async {
    Weather w = await wf.currentWeatherByCityName("Krasnoyarsk");
    return w.temperature?.celsius!.toInt().toString();
  }

  @override
  Widget build(BuildContext context) {

    final theme = Theme.of(context);
    
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("ГЛАВНАЯ"),
      ),
      body: Stack(
        children: [
          ListView(
            children: [
              const SizedBox(height: 60),

              getRowBtn(
                const ImageIcon(AssetImage('assets/icon_map.png'), size: 55, color: Colors.black),
                '/map',
                context
              ),
              const SizedBox(height: 7),
              getCenterText('КАРТА'),
              const SizedBox(height: 40),

              getRowBtn(
                const ImageIcon(AssetImage('assets/icon_note.png'), size: 55, color: Colors.black),
                '/notebook',
                context
              ),
              const SizedBox(height: 7),
              getCenterText('БЛОКНОТ'),
              const SizedBox(height: 40),

              getRowBtn(
                const ImageIcon(AssetImage('assets/icon_dino.png'), size: 55, color: Colors.black),
                '/game',
                context
              ),
              const SizedBox(height: 7),
              getCenterText('ИГРА'),
              const SizedBox(height: 40),
            ],
          ),

          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "ПОГОДА",
                style: theme.appBarTheme.titleTextStyle
              ),
              const SizedBox(height: 7),
              Container(
                color: mainBlue,
                height: 135,
                width: MediaQuery.of(context).size.width,
                
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          children: const <Widget>[
                            Icon(
                              Icons.cloud,
                              color: Colors.black,
                              size: 55,
                            ),
                          ],
                        ),

                        const SizedBox(width: 70),

                        Column(
                          children: <Widget>[
                            const Text('Красноярск'),
                            const SizedBox(height: 10),
                            Container(
                              color: Colors.black,
                              width: 170,
                              height: 3,
                            ),
                            const SizedBox(height: 10),
                            FutureBuilder(
                              future: readNode(),
                              initialData: "Загрузка...",
                              builder: (BuildContext context, AsyncSnapshot<String?> text) {
                                return Text('${text.data} С');
                              }
                            )
                          ]
                        ),
                        
                      ],
                    ),
                  ],
                ),
              
              ),
            ],
          ),

        ],
      ),
    );
  }

  /* Функция получения списка кнопок для главной странице. */
  Widget getRowBtn(ImageIcon icon, String routeName, BuildContext context) {

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[

        SizedBox(
          width: 80,
          height: 80,
          child: FloatingActionButton(
            heroTag: routeName,
            onPressed: () {
              //Navigator.of(context).pushNamed(routeName);
              Navigator.pushReplacementNamed(context, routeName);
            },
            backgroundColor: mainOrange,
            shape: BeveledRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: icon,
          ),
        ),

      ],
    );
  }

  /* Функция получения строки по центру экрана. */
  Widget getCenterText(String text) {
    return Align(
      alignment: Alignment.center,
      child: Text(text),
    );
  }

}
