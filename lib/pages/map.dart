import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';


class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  State<MapPage> createState() => _MapPageState();
}


class _MapPageState extends State<MapPage> {

  @override
  Widget build(BuildContext context) {

    const urlTemplate = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
    // const urlTemplate = 'https://stamen-tiles.a.ssl.fastly.net/toner-background/{z}/{x}/{y}.png';
    
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("КАРТА"),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            size: 35,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/');
          }
        ),
      ),

      body: FlutterMap(
        options: MapOptions(
          center: LatLng(55.99443, 92.79742),
          zoom: 16,
        ),
        children: [
          TileLayer(
              urlTemplate: urlTemplate,
              userAgentPackageName: 'com.example.practice',
          ),

          MarkerLayer(
            markers: [
              Marker(
                point: LatLng(55.99443, 92.79742),
                builder: (context) => const ImageIcon(AssetImage('assets/it.png'))
              )
            ],
          ),
        ],
      ),
    );
  }
}
