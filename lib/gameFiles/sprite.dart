class Sprite {
  String imagePath;
  int imageWidth;
  int imageHeight;

  Sprite(this.imagePath, this.imageWidth, this.imageHeight);
}
