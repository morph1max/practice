import 'package:flutter/material.dart';
import 'package:practice/const.dart';
import 'package:practice/gameFiles/gameObjects/gameObject.dart';


class Cactus extends GameObject {

  Offset location;  // Позиция кактуса.
 
  Cactus(this.location);
 
  @override
  Rect getRect(Size screenSize, double runDistance) {
    return Rect.fromLTWH(
        (location.dx - runDistance) * SPEED_CACTUS,
        4 / 7 * screenSize.height - spriteCactus.imageHeight + 39,
        spriteCactus.imageWidth.toDouble(),
        spriteCactus.imageHeight.toDouble());
  }
 
  @override
  Widget render() {
    return Image.asset(
      spriteCactus.imagePath,
    );
  }
}