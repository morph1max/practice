import 'package:flutter/material.dart';
import 'package:practice/const.dart';
import 'package:practice/gameFiles/gameObjects/gameObject.dart';


enum DinoState {
  running,
  jumping,
  dead,
}


class Dino extends GameObject {

  int frame = 1;
  double displayCurrentY = 0;
  double currentY = 0;

  DinoState state = DinoState.running;
 
  @override
  Widget render() {
    return Image.asset(
      "assets/dino_$frame.png",
      gaplessPlayback: true,
    );
  }
 
  @override
  Rect getRect(Size screenSize, double _) {
    return Rect.fromLTWH(
        screenSize.width / 10,
        4 / 7 * screenSize.height - dino.imageHeight - displayCurrentY,
        dino.imageWidth.toDouble(),
        dino.imageHeight.toDouble());
  }

  @override
  void update(Duration lastTime, Duration? elapsedTime) {
    double elapsedSeconds =
      ((elapsedTime!.inMilliseconds - lastTime.inMilliseconds) / 1000);
 
    displayCurrentY += currentY * elapsedSeconds;
    if (displayCurrentY <= 0) {
      displayCurrentY = 0;
      currentY = 0;
      state = DinoState.running;
    } else {
      currentY -= GRAVITY_KOEF * elapsedSeconds;
    }
    
    frame = (elapsedTime.inMilliseconds / 100).floor() % 3 + 1;
  }

  void jump() {
    if (state != DinoState.jumping) {
      currentY = JUMP_HEIGHT;
      state = DinoState.jumping;
    }
  }

  void die() {
    state = DinoState.dead;
    frame = 4;
  }
}
